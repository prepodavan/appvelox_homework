from img_resizer import create_app
from img_resizer.tasks.database import init_db as tasks_init_db


app = create_app()


@app.cli.command('createdb')
def init_db():
    tasks_init_db()

