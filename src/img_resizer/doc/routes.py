from . import app
import flask


@app.route('/', methods=['GET'])
def index():
    return flask.redirect(flask.url_for(f'{app.name}.static', filename='/openapi.html'))

