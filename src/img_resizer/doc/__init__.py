from pathlib import Path
from flask import Blueprint


static_dir_path = Path(__file__).parent / "static"
if not static_dir_path.exists():
    static_dir_path.mkdir()

app = Blueprint('doc', __name__, static_url_path='', static_folder=f'{static_dir_path}')

from . import routes

