from pathlib import Path
from flask import Flask
from img_resizer.shared.extensions import init_extensions
from img_resizer.errors_handling import init_errors_handlers
from img_resizer.blueprints import init_blueprints


def create_app(config_file_path: Path = Path(__file__).parent / 'default_settings.py') -> Flask:
    app = Flask(__name__)
    app.config.from_pyfile(config_file_path)
    init_extensions(app)
    init_errors_handlers(app)
    init_blueprints(app)

    return app


from img_resizer import routes

