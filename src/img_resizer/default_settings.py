import os
from pathlib import Path


def storage_dir() -> Path:
    provided = os.environ.get('STORAGE_DIR_PATH')
    if provided:
        return Path(provided)
    return Path(__file__).parent / 'files_storage' / 'default_storage_dir'


FILES_STORAGE = {
    'ALLOWED_EXT': {'jpg', 'png'},
    'MAX_FILE_SIZE': 1024 * 1024,
    'STORAGE_DIR_PATH': storage_dir(),
}

if not FILES_STORAGE['STORAGE_DIR_PATH'].parent.exists():
    FILES_STORAGE['STORAGE_DIR_PATH'].parent.mkdir()
if not FILES_STORAGE['STORAGE_DIR_PATH'].exists():
    FILES_STORAGE['STORAGE_DIR_PATH'].mkdir()

RESIZE_TASKS = {
    'MAX_HEIGHT': 9999,
    'MAX_WIDTH': 9999,
}

SQLALCHEMY_DATABASE_URI = os.environ['SQLALCHEMY_DATABASE_URI']
SQLALCHEMY_TRACK_MODIFICATIONS = False
CELERY_BROKER_URL = os.environ['CELERY_BROKER_URL']
CELERY_RESULT_BACKEND = os.environ['CELERY_RESULT_BACKEND']

