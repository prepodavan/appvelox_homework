from img_resizer.shared import db
from flask import Flask, jsonify
from img_resizer.shared.exceptions import InvalidUsage


def init_errors_handlers(app: Flask):
    @app.errorhandler(InvalidUsage)
    def catch_invalid_usage(invalid_usage: InvalidUsage):
        kw = {
            'message': invalid_usage.message,
        }

        if invalid_usage.payload is not None:
            kw['payload'] = invalid_usage.payload

        return jsonify(**kw), invalid_usage.status_code

    @app.errorhandler(500)
    def catch_internal(error):
        db.session.rollback()
        return jsonify(), 500

