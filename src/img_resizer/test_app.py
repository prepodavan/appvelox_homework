import io
import shutil
import tempfile
import uuid
import unittest
import datetime
from unittest.mock import MagicMock
from typing import Optional
from flask_testing import TestCase
from flask import Flask, url_for
from pathlib import Path
from alchemy_mock.mocking import UnifiedAlchemyMagicMock
from img_resizer import init_errors_handlers
from img_resizer import init_blueprints
from img_resizer.shared import db
from img_resizer.tasks.models import Task
from img_resizer.shared import celery as celery_app


class AppConfig:
    TESTING = True
    DEBUG = False
    TMP_DIR = Path(tempfile.mkdtemp())
    FILES_STORAGE = {
        'ALLOWED_EXT': {'jpg', 'png'},
        'MAX_FILE_SIZE': 1024 * 1024,
        'STORAGE_DIR_PATH': TMP_DIR / 'files_storage'
    }

    RESIZE_TASKS = {
        'MAX_HEIGHT': 9999,
        'MAX_WIDTH': 9999,
    }

    CELERY_BROKER_URL = "memory://"
    CELERY_BROKER_BACKEND = "memory"
    CELERY_RESULTS_BACKEND_PATH = TMP_DIR / 'celery.results.sqlite'
    CELERY_RESULTS_BACKEND = f"db+sqlite:///{CELERY_RESULTS_BACKEND_PATH}"

    def __init__(self, *args, **kw) -> None:
        if not self.TMP_DIR.exists():
            self.TMP_DIR.mkdir()
        if not self.FILES_STORAGE['STORAGE_DIR_PATH'].exists():
            self.FILES_STORAGE['STORAGE_DIR_PATH'].mkdir()
        if not self.CELERY_RESULTS_BACKEND_PATH.exists():
            self.CELERY_RESULTS_BACKEND_PATH.touch()
        super().__init__(*args, **kw)


class FlaskAppTestCase(TestCase):
    def create_app(self) -> Flask:
        app = Flask(__name__)
        app.config.from_object(AppConfig())
        init_errors_handlers(app)
        init_blueprints(app)
        self.init_celery(app)
        return app

    def init_celery(self, app: Flask):
        celery_app.conf.broker_url = app.config['CELERY_BROKER_URL']
        celery_app.conf.broker_backend = app.config['CELERY_BROKER_BACKEND']
        celery_app.conf.result_backend = app.config['CELERY_RESULTS_BACKEND']

    @classmethod
    def tearDownClass(cls) -> None:
        shutil.rmtree(str(AppConfig().TMP_DIR))
        super().tearDownClass()

    def assert_errors_messages(self, response, required_errors):
        errors = response.json.get('message')
        self.assertTrue(errors is not None)
        self.assertTrue(isinstance(errors, list))
        self.assertGreater(len(errors), 0)

        self.assertTrue(list(required_errors) == errors)


class PostingTaskTestCase(FlaskAppTestCase):
    FILENAME = f'test.{list(AppConfig().FILES_STORAGE["ALLOWED_EXT"])[0]}'

    @staticmethod
    def make_task_data(height=1, width=1, file=None, filename=FILENAME) -> dict:
        return {
            'height': height,
            'width': width,
            'file': (file if file is not None else io.BytesIO(b'hello'), filename),
        }

    def post_task(self, task=None):
        return self.client.post(url_for('tasks.start_new_task'),
                                data=task if task is not None else self.make_task_data())

    def test_task_successfully_started(self):
        Task.query = db.session = UnifiedAlchemyMagicMock()

        task = self.make_task_data()
        res = self.post_task(task=task)
        self.assert200(res)

        all_tasks = db.session.query(Task).all()
        self.assertEqual(len(all_tasks), 1)
        self.assertEqual(all_tasks[0].file_extension, task['file'][1].rsplit('.')[1])
        self.assertEqual(all_tasks[0].width, task['width'])
        self.assertEqual(all_tasks[0].height, task['height'])
        self.assertTrue(all_tasks[0].finished_at is None)
        self.assertTrue(all_tasks[0].dest_uuid is None)

    def test_schema(self):
        res = self.post_task(task=dict())
        self.assert400(res)
        self.assert_errors_messages(res, ['This field is required.'] * 3)

        res = self.post_task(task=self.make_task_data(
            height=AppConfig().RESIZE_TASKS['MAX_HEIGHT'] + 1,
            width=AppConfig().RESIZE_TASKS['MAX_WIDTH'] + 1
        ))
        self.assert400(res)
        self.assert_errors_messages(res, [
            "'height' is not in range(1, 9999)",
            "'width' is not in range(1, 9999)",
        ])

        res = self.post_task(task=self.make_task_data(0, 0))
        self.assert400(res)
        self.assert_errors_messages(res, [
            "'height' is not in range(1, 9999)",
            "'width' is not in range(1, 9999)",
        ])

        big_str = '1' * (AppConfig().FILES_STORAGE['MAX_FILE_SIZE'] + 1)
        big_file = io.BytesIO(bytes(big_str, encoding='ascii'))
        res = self.post_task(task=self.make_task_data(file=big_file))
        self.assert400(res)
        self.assert_errors_messages(res, [f'max size of file: {AppConfig().FILES_STORAGE["MAX_FILE_SIZE"]}'])

        res = self.post_task(task=self.make_task_data(filename='invalid.txt'))
        self.assert400(res)
        self.assert_errors_messages(res, [f"file should be one of: {AppConfig().FILES_STORAGE['ALLOWED_EXT']}"])


class TaskGetterTestUtils:
    @staticmethod
    def mock_task_query(task: Optional[Task]) -> MagicMock:
        Task.query = UnifiedAlchemyMagicMock()
        magic_mock = MagicMock(return_value=task)
        Task.query.get = magic_mock
        return magic_mock


class GettingTaskStatusTestCase(FlaskAppTestCase, TaskGetterTestUtils):
    def assert_task_status(self, res, status: str):
        self.assertTrue(res.json is not None)
        self.assertTrue(res.json.get('status') is not None)
        self.assertEqual(res.json['status'], status)

    def test_processing(self):
        task = Task(id=1)
        magic_mock = self.mock_task_query(task)
        res = self.client.get(f'/tasks/{task.id}/status')
        self.assert200(res)
        magic_mock.assert_called_with(task.id)
        self.assert_task_status(res, 'processing')

    # TODO: test killed

    def test_done(self):
        task = Task(id=1, finished_at=datetime.datetime.utcnow())
        magic_mock = self.mock_task_query(task)
        res = self.client.get(f'/tasks/{task.id}/status')
        self.assert200(res)
        magic_mock.assert_called_with(task.id)
        self.assert_task_status(res, 'done')


class GettingTaskResultTestCase(FlaskAppTestCase, TaskGetterTestUtils):
    def create_img_file(self, file_id: str = uuid.uuid4().urn[9:]) -> Path:
        ext = list(AppConfig().FILES_STORAGE['ALLOWED_EXT'])[0]
        dir_path = AppConfig().FILES_STORAGE['STORAGE_DIR_PATH']
        file_path = dir_path / f"{file_id}.{ext}"
        file_path.touch()
        return file_path

    def test_found(self):
        content = b'hello world'
        task = Task(id=4, finished_at=datetime.datetime.utcnow(), dest_uuid=uuid.uuid4())
        file_path = self.create_img_file(task.dest_uuid)
        file_path.open('wb').write(content)
        magic_mock = self.mock_task_query(task)
        res = self.client.get(f'/tasks/{task.id}/result')
        self.assert200(res)
        magic_mock.assert_called_with(task.id)
        self.assertEqual(res.data, content)

    def test_not_found(self):
        # task=None
        task_id = 1
        magic_mock = self.mock_task_query(None)
        res = self.client.get(f'/tasks/{task_id}/result')
        self.assert404(res)
        magic_mock.assert_called_with(task_id)

        tasks = [
            Task(id=1),
            Task(id=2, finished_at=datetime.datetime.utcnow()),
            Task(id=3, dest_uuid=uuid.uuid4()),
        ]

        for task in tasks:
            magic_mock = self.mock_task_query(task)
            res = self.client.get(f'/tasks/{task.id}/result')
            self.assert404(res)
            magic_mock.assert_called_with(task.id)


if __name__ == '__main__':
    unittest.main()
