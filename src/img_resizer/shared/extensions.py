from flask import Flask
from img_resizer.shared import db
from . import celery
from . import migrate


def init_celery(app: Flask):
    celery.conf.broker_url = app.config["CELERY_BROKER_URL"]
    celery.conf.result_backend = app.config["CELERY_RESULT_BACKEND"]
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        """Make celery tasks work with Flask app context"""

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask


def init_extensions(app: Flask):
    db.init_app(app)
    migrate.init_app(app, db)
    init_celery(app)

