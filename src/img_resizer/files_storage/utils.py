import uuid
from typing import Optional
from pathlib import Path
from flask import current_app


def allowed_file(filename: str) -> bool:
    return '.' in filename and \
           filename.rsplit('.')[1] in current_app.config['FILES_STORAGE']['ALLOWED_EXT']


def get_file_path(uuid_str: str) -> Optional[Path]:
    storage_path = Path(current_app.config['FILES_STORAGE']['STORAGE_DIR_PATH'])
    glob = [x for x in storage_path.glob(f'{uuid_str}*')]
    if len(glob) == 0:
        return None
    return glob[0]


def save_file(filename: str, file_bytes: bytes) -> uuid.UUID:
    file_id = uuid.uuid4()
    extension = filename.rsplit('.')[1]
    storage_path = current_app.config['FILES_STORAGE']['STORAGE_DIR_PATH']
    file_path = Path(storage_path) / f'{file_id}.{extension}'
    file_path.open('wb').write(file_bytes)
    return file_id

