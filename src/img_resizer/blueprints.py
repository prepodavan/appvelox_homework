from flask import Flask
from img_resizer import tasks
from img_resizer import doc


def init_blueprints(app: Flask):
    app.register_blueprint(tasks.app, url_prefix='/tasks')
    app.register_blueprint(doc.app, url_prefix='/doc')
    return app

