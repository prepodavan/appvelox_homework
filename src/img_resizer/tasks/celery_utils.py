import uuid
import datetime
from pathlib import Path
from PIL import Image
from img_resizer.shared import celery
from img_resizer.shared import db
from . import models


@celery.task
def run_resizing(task_id: int, height: int, width: int, source_file_path: str) -> str:
    source_path = Path(source_file_path)
    resized_img = Image.open(source_file_path).resize((height, width))
    dest_uuid = uuid.uuid4()
    dest_path = str(source_path.parent / f'{dest_uuid}.{source_path.suffix[1:]}')
    resized_img.save(dest_path)
    models.Task.query.filter(models.Task.id == task_id).update(
        {
            'dest_uuid': dest_uuid,
            'finished_at': datetime.datetime.utcnow(),
        },
    )
    db.session.commit()
    return dest_path

