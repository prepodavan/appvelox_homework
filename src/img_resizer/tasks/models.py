import datetime
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy import Column, Integer, DateTime, String
from .database import Base


class Task(Base):
    __tablename__ = 'tasks'
    id = Column(Integer, primary_key=True)
    height = Column(Integer, nullable=False)
    width = Column(Integer, nullable=False)
    source_uuid = Column(UUID(as_uuid=True), nullable=False)
    dest_uuid = Column(UUID(as_uuid=True), default=None, nullable=True)
    file_extension = Column(String, nullable=False)
    started_at = Column(DateTime, default=datetime.datetime.utcnow, nullable=False)
    finished_at = Column(DateTime, default=None, nullable=True)

    def __str__(self):
        return f'Task(id={self.id}, height={self.height}, width={self.width},' \
               f' started_at={self.started_at}, finished_at={self.finished_at},' \
               f' source_uuid={self.source_uuid}, dest_uuid={self.dest_uuid}, self.ext={self.file_extension})'

