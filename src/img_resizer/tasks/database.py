from flask_sqlalchemy import declarative_base
from img_resizer.shared import db


Base = declarative_base()
Base.query = db.session.query_property()


def init_db():
    Base.metadata.create_all(bind=db.engine)

