from flask import Blueprint


app = Blueprint('tasks', __name__)

from . import routes
from . import database

