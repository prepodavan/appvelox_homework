import flask
from img_resizer.shared import db
from img_resizer.shared.exceptions import InvalidUsage
from img_resizer.files_storage.utils import get_file_path, save_file
from .input_validation import NewTaskSchema, GetTaskSchema, GetTaskResultSchema
from .celery_utils import run_resizing
from img_resizer.shared import celery as celery_app
from . import app
from . import models
from celery.result import AsyncResult
from celery import states as celery_states


PROCESSING_STATES = {
    celery_states.PENDING,
    celery_states.STARTED,
}


@app.route('/', methods=['POST'])
def start_new_task():
    schema = NewTaskSchema(flask.request)
    if not schema.validate():
        raise InvalidUsage(schema.errors)
    max_file_size = flask.current_app.config['FILES_STORAGE']['MAX_FILE_SIZE'] + 1
    file = flask.request.files['file']
    file_bytes = file.read(max_file_size)
    if len(file_bytes) == max_file_size:
        raise InvalidUsage([f'max size of file: {max_file_size - 1}'])

    source_uuid = save_file(file.filename, file_bytes)
    source_file = get_file_path(source_uuid.urn[9:])
    task = models.Task(
        height=int(flask.request.form['height']),
        width=int(flask.request.form['width']),
        source_uuid=source_uuid,
        file_extension=source_file.suffix[1:]
    )
    db.session.add(task)
    db.session.commit()
    celery_task_args = (task.id, task.height, task.width, str(source_file))
    run_resizing.apply_async(celery_task_args, task_id=str(task.id))
    return flask.jsonify(task_id=task.id)


@app.route('/<int:task_id>/status', methods=['GET'])
def get_task_status(task_id: int):
    schema = GetTaskSchema(flask.request)
    if not schema.validate():
        raise InvalidUsage(schema.errors, status_code=404)
    task = models.Task.query.get(task_id)
    if task.finished_at is not None:
        return flask.jsonify(status='done')
    if AsyncResult(str(task.id), app=celery_app).state in PROCESSING_STATES:
        return flask.jsonify(status='processing')
    return flask.jsonify(status='killed')


@app.route('/<int:task_id>/result', methods=['GET'])
def get_task_result(task_id: int):
    schema = GetTaskResultSchema(flask.request)
    if not schema.validate():
        raise InvalidUsage(schema.errors, status_code=404)
    task = models.Task.query.get(task_id)
    file_path = get_file_path(task.dest_uuid.urn[9:])
    return flask.send_file(file_path, as_attachment=True)

