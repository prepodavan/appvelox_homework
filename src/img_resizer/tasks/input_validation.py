import flask
from flask import current_app
from flask_inputs import Inputs
from wtforms.validators import DataRequired, ValidationError
from .models import Task


def int_type(form, field):
    try:
        int(field.data)
    except ValueError:
        raise ValidationError(f"'{field.name}' is not int")


def resizing_new_sizes(form, field):
    val = int(field.data)
    max_size = current_app.config['RESIZE_TASKS'][f'MAX_{field.name.upper()}']
    if not (0 < val < max_size):
        raise ValidationError(f"'{field.name}' is not in range(1, {max_size})")


def allowed_file(*args, **kw):
    allowed = current_app.config['FILES_STORAGE']['ALLOWED_EXT']
    filename = flask.request.files['file'].filename
    if '.' in filename and filename.rsplit('.')[1] in allowed:
        return
    raise ValidationError(f'file should be one of: {allowed}')


def task_exists(form, field) -> Task:
    task = Task.query.get(field.data)
    if task is None:
        raise ValidationError('Task does not exists')
    return task


def finished_task_exists(form, field):
    task = task_exists(form, field)
    if task.finished_at is None or task.dest_uuid is None:
        raise ValidationError('Task does not exists')


class NewTaskSchema(Inputs):
    valid_attrs = ['files', 'form']

    files = {
        'file': [DataRequired(), allowed_file],
    }

    form = {
        'height': [DataRequired(), int_type, resizing_new_sizes],
        'width': [DataRequired(), int_type, resizing_new_sizes],
    }


class GetTaskSchema(Inputs):
    rule = {
        'task_id': [DataRequired(), task_exists]
    }


class GetTaskResultSchema(Inputs):
    rule = {
        'task_id': [DataRequired(), finished_task_exists]
    }

