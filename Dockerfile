FROM python:3.8-alpine as base

FROM base as builder
COPY requirements.txt /requirements.txt
RUN mkdir /install && mkdir /install/pip

# runtime deps
RUN apk update && apk add --no-cache libjpeg postgresql-libs

# build deps
RUN apk add --no-cache --virtual .build-deps jpeg-dev zlib-dev gcc musl-dev postgresql-dev && \
    python -m pip install -U --force-reinstall pip && \
    python -m pip install  --prefix=/install/pip -r /requirements.txt && \
    apk --purge del .build-deps

FROM base
COPY --from=builder /install/pip /usr/local
COPY --from=builder /usr/lib /usr/lib
COPY src /app
WORKDIR /app
ENTRYPOINT []