# API Design
В исходную задача входила подзадача "Путь для постановки задачи на изменение размера",
что подразумевает, что файл либо находится уже в системе, либо будет передан вместе с запросом на изменение.
Сценарии, где система умеет изменять только файлы, данные ей при инициализации, я не рассматриваю.
Следовательно нам нужно предоставить способ занести файл в систему.
## Task posting
Передавать файл я считаю нужным через форму, чтобы не приходилось перекодировать файл для отправки в base64
(чего потребовал способ передачи через json, xml, ...).
При этом мы уже не можем передовать параметры изменения размера файла в более привычных application Content-Type'ах.
Передавать их в uri - противоречит rest http api, так как такой uri не будет соответствовать ресурсу.
Передавать высоту и ширину через query будет яв-ся не лучшей практикой, так как не соответствует семантика query-string.
Поэтому эти параметры так же будут переданы через форму.
## Task result resource
В исходную задачу не входил путь для получения результата resizing'а, но я реализовал его для удобства.

# Technologies
## Flask
Выбор веб фреймворка зависел исключительно от того, насколько хорошо я его знаю
## Celery
Можно было бы и просто redis queue, но celery имеет более широкий функционал, на всякий случай предпочитаю брать ее
## Postgresql
NoSQL решения, я считаю, надо брать, когда оперируете большими обемами данных. В задачу это не входило.

SQLite не подходит, так как требуется доступ к базе из worker'ов celery

Redis подошел бы и был бы более простым в использовании и давал бы большую скорость.
Но с sql базой данных получаются такие возможности, как:

1)  Возможность перезапуска задачи, в случае ошибки.
2) Возможность отслеживать статус задач, выполненных во время предыдущего запуска сервера.
