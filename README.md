#Homework for appvelox from kars-v@bk.ru

## Installation and Usage
`docker-compose up --build`

## Flow
1) Upload file and save id, if u have no id of existing (in service) file
2) Start task for resizing
3) Now u can check status of task and result of resizing if it's done

## Summary
Take a look onto [Open Api Specification](https://veselisksw.herokuapp.com/) of this service

And also take a look onto [reasons of preferences of technologies and architecture](REASONS.md)